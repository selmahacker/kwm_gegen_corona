import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): any {
    let dateParts = value.split(' ')[0].split('-');
    let date = dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0];
    return date;
  }

}
