export interface User {
    id: number;
    first_name: string;
    last_name: string;
    role: number;
    email: string;
    email_verified_at?: any;
    created_at?: any;
    updated_at?: any;
    street: string;
    post_code: number;
    city: string;
}