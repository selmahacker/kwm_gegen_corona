import { ShoppingItem } from './shopping-item.model';
import { User } from './user.model';

export interface ShoppingList {
    id: number;
    created_at: string;
    updated_at: string;
    fullfillment_date: string;
    paid_price?: any;
    owner_user_id: number;
    volunteer_user_id?: any;
    shopping_items?: ShoppingItem[];
    owner?: User;
    volunteer?: User;
    state: State;
    comments: Comment[]
}

export enum State {
    offen = 'offen', uebernommen = 'uebernommen', zugestellt = 'zugestellt'
}