export interface Comment {
    user_id: number,
    content: string,
}