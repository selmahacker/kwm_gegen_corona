export interface ShoppingItem {
    id: number;
    created_at: string;
    updated_at: string;
    description: string;
    amount: number;
    max_cost: string;
    shopping_list_id: number;
}