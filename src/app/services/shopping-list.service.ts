import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ShoppingList, State } from '../shared/shopping-list.model';
import { AuthService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  private api = 'http://corona.s1710456009.student.kwmhgb.at/api';
  
  constructor(private http: HttpClient, private authService: AuthService) { }

  getAll(): Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/shopping_lists`).pipe(
      retry(3)
    ).pipe(
      catchError(this.errorHandler)
    );
  }

  getSingle(id: string): Observable<ShoppingList> {
    return this.http.get(`${this.api}/shopping_lists/${id}`).pipe(
      retry(3)
    ).pipe(
      catchError(this.errorHandler)
    );
  }

  saveList(list: ShoppingList): Observable<ShoppingList> {
    console.log('service', list);
    return this.http.post(`${this.api}/shopping_lists`, list).pipe(
    ).pipe(
      catchError(this.errorHandler)
    );
  }

  delete(id: string) {
    return this.http.delete(`${this.api}/shopping_lists/${id}`).pipe(
      catchError(this.errorHandler)
    )
  }

  acceptList(id: string) {
    let userId = localStorage.getItem('userId');
    if(!userId) {
      throw new Error('User not logged in!');
    }
    let changes = {volunteer_user_id: userId, state: 'uebernommen'};
    return this.http.put(`${this.api}/shopping_lists/${id}`, changes).pipe(
      catchError(this.errorHandler)
    );
  }

  orderShipped(id: string) {
    let userId = localStorage.getItem('userId');
    if(!userId) {
      throw new Error('User not logged in!');
    }
    let changes = {state: 'zugestellt'};
    return this.http.put(`${this.api}/shopping_lists/${id}`, changes).pipe(
      catchError(this.errorHandler)
    );
  }

  private errorHandler(error: Error | any): Observable<any> {
    return throwError(error);
  }
}
