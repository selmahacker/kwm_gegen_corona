import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ShoppingItem } from '../shared/shopping-item.model';
import { catchError, retry, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ShoppingItemService {
  private api = 'http://corona.s1710456009.student.kwmhgb.at/api';
  constructor(private http: HttpClient) { }

  getAll(listId: string): Observable<Array<ShoppingItem>> {
    return this.http.get(`${this.api}/shopping_lists/${listId}/shopping_items`).pipe(
      retry(3),
      tap(console.log)
    ).pipe(
      catchError(this.errorHandler)
    );
  }

  addItem(listId: string, item: ShoppingItem) {
    return this.http.post(`${this.api}/shopping_lists/${listId}/shopping_items`, item).pipe(
      retry(3),
    ).pipe(
      catchError(this.errorHandler)
    );
  }

  deleteItem(listId: string, id: number) {
    return this.http.delete(`${this.api}/shopping_lists/${listId}/shopping_items/${id}`)
  }

  private errorHandler(error: Error | any): Observable<any> {
    return throwError(error);
  }
}
