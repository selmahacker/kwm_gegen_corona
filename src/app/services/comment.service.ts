import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../shared/comment.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private api = 'http://corona.s1710456009.student.kwmhgb.at/api';
  
  constructor(private http: HttpClient) { }

  saveComment(listId: number, comment: Comment): Observable<any> {
    return this.http.post(`${this.api}/shopping_list/${listId}/comments`, comment);
  }
}
