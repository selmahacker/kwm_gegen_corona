import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ShoppingListService } from '../services/shopping-list.service';
import { AuthService } from '../services/authentication.service';
import { ShoppingList } from '../shared/shopping-list.model';

@Component({
  selector: 'co-list-form',
  templateUrl: './list-form.component.html',
  styles: []
})
export class ListFormComponent implements OnInit {
  edit = false;
  listForm: FormGroup;
  list: ShoppingList;
  constructor(private fb: FormBuilder, private listService: ShoppingListService, private authService: AuthService) { }

  ngOnInit(): void {
    this.listForm = this.fb.group({
      owner_user_id: [this.authService.getCurrentUserId()],
      fullfillment_date: ['', Validators.required]
    });
  }

  submitForm() {
    console.log(this.listForm.value);
    this.listService.saveList(this.listForm.value).subscribe(list => this.list = list);
  }

}
