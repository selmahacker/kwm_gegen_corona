import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListsComponent } from './shopping-lists/shopping-lists.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { LoginComponent } from './login/login.component';
import { MyListsComponent } from './my-lists/my-lists.component';
import { EditListComponent } from './edit-list/edit-list.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'shoppingLists', component: ShoppingListsComponent},
  { path: 'shoppingLists/:listId', component: ShoppingListComponent},
  { path: 'my-lists', component: MyListsComponent},
  { path: 'shoppingLists/:listId/edit', component: EditListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
