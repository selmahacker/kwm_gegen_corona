import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingListsComponent } from './shopping-lists/shopping-lists.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthService } from './services/authentication.service';
import { ShoppingItemService } from './services/shopping-item.service';
import { ShoppingListService } from './services/shopping-list.service';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { JwtInterceptorService } from './services/jwt-interceptor.service';
import { DatePipe } from './shared/date.pipe';
import { MyListsComponent } from './my-lists/my-lists.component';
import { ListFormComponent } from './list-form/list-form.component';
import { EditListComponent } from './edit-list/edit-list.component';
import { CommentComponent } from './comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    ShoppingListComponent,
    ShoppingListsComponent,
    LoginComponent,
    DatePipe,
    MyListsComponent,
    ListFormComponent,
    EditListComponent,
    CommentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [
    AuthService,
    ShoppingItemService,
    ShoppingListService, 
    {
      provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
