import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../shared/comment.model'
import { AuthService } from '../services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommentService } from '../services/comment.service';

@Component({
  selector: 'co-comment',
  templateUrl: './comment.component.html',
  styles: []
})
export class CommentComponent implements OnInit {
  @Input() comments: Comment[];
  @Input() listId: number;
  userId: number;
  loading = false;

  commentForm: FormGroup;

  constructor(private authService: AuthService, private fb: FormBuilder, private commentService: CommentService) { }

  ngOnInit(): void {
    console.log(this.listId, this.comments);
    
    this.userId = this.authService.getCurrentUserId();
    this.commentForm = this.fb.group({
      user_id: [this.userId],
      content: ['', Validators.required]
    });
  }

  submitComment() {
    this.loading = true;
    this.commentService.saveComment(this.listId, this.commentForm.value).subscribe(
      comment => {
        console.log(comment);
        if (!this.comments) {
          this.comments = [];
        }
        this.comments.push(comment);
        this.loading = false;
      }
    );
    this.commentForm.reset({user_id: this.authService.getCurrentUserId()});
  }

}
