import { Component, OnInit } from '@angular/core';
import { ShoppingListService } from '../services/shopping-list.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/authentication.service';
import { take } from 'rxjs/operators';
import { ShoppingList } from '../shared/shopping-list.model';

@Component({
  selector: 'co-edit-list',
  templateUrl: './edit-list.component.html',
  styles: []
})
export class EditListComponent implements OnInit {
listId: string;
list: ShoppingList;

  constructor(private shoppingListService: ShoppingListService,
    private route: ActivatedRoute,
    private authService: AuthService,) { }

  ngOnInit(): void {
    this.listId = this.route.snapshot.paramMap.get('listId');
      this.updateList();
  }

  updateList() {
    this.shoppingListService.getSingle(this.listId)
      .pipe(
        take(1)
      ).subscribe(list => this.list = list);
  }



}
