import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShoppingItemService } from '../services/shopping-item.service';
import { Observable } from 'rxjs';
import { ShoppingItem } from '../shared/shopping-item.model';
import { ActivatedRoute } from '@angular/router';
import { ShoppingListService } from '../services/shopping-list.service';
import { ShoppingList, State } from '../shared/shopping-list.model';
import { AuthService } from '../services/authentication.service';
import { take } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'co-shopping-list',
  templateUrl: './shopping-list.component.html',
  styles: []
})
export class ShoppingListComponent implements OnInit {
  listId: string;
  @Output() deleted = new EventEmitter();
  @Input() list: ShoppingList;
  @Input() edit = false;
  State = State;
  addItemForm: FormGroup;
  
  constructor(
    private shoppingListService: ShoppingListService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private fb: FormBuilder,
    private itemService: ShoppingItemService,
    ) { }

  ngOnInit(): void {
    this.addItemForm = this.fb.group({
      description: ['', Validators.required],
      amount: ['', Validators.required],
      max_cost: ['', Validators.required]
    });
    if(!this.list) {
      this.listId = this.route.snapshot.paramMap.get('listId');
      this.updateList();
    } else {
      this.listId = this.list.id.toString();
    }
  }

  updateList() {
    this.shoppingListService.getSingle(this.listId)
      .pipe(
        take(1)
      ).subscribe(list => this.list = list);
  }

  acceptList() {
    this.shoppingListService.acceptList(this.listId)
      .subscribe(_ => this.updateList());
  }

  orderShipped() {
    this.shoppingListService.orderShipped(this.listId)
      .subscribe(_ => this.updateList());
  }

  deleteList() {
    this.shoppingListService.delete(this.listId).subscribe(_ => {
      this.deleted.emit();
    });
  }

  addItem() {
    console.log('add Item');
    console.log(this.addItemForm.value);
    this.itemService.addItem(this.listId, this.addItemForm.value).pipe(
      take(1)
    ).subscribe(_ => {
      this.updateList();
      this.addItemForm.reset();
    });
  }

  deleteItem(id: number) {
    console.log('delete Item number ' + id);
    this.itemService.deleteItem(this.listId, id).pipe(
      take(1)
      ).subscribe(_ => this.updateList());
  }

  get role() {
    return this.authService.getCurrentUserRole();
  }

}
