import { Component } from '@angular/core';
import { AuthService } from './services/authentication.service';

@Component({
  selector: 'co-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'corona';

  constructor (private authService: AuthService) {}
  
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  getLoginLabel() {
    if(this.isLoggedIn()) {
      return 'Logout';
    } else {
      return 'Login';
    }
  }
}
