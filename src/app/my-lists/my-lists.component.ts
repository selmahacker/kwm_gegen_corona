import { Component, OnInit } from '@angular/core';
import { ShoppingListService } from '../services/shopping-list.service';
import { Observable } from 'rxjs';
import { ShoppingList } from '../shared/shopping-list.model';
import { map, tap } from 'rxjs/operators';
import { AuthService } from '../services/authentication.service';

@Component({
  selector: 'co-my-lists',
  templateUrl: './my-lists.component.html',
  styles: []
})
export class MyListsComponent implements OnInit {

  lists: Observable<ShoppingList[]>;

  constructor(private listService: ShoppingListService, private authService: AuthService) { }

  ngOnInit(): void {
    this.updateLists();
  }

  updateLists() {
    let userId = this.authService.getCurrentUserId();
    this.lists = this.listService.getAll().pipe(
      map(lists => lists.filter(list => list.owner.id === userId || list.volunteer?.id === userId)),
    );
  }

}
