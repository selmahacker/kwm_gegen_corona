import { Component, OnInit } from '@angular/core';
import { ShoppingListService } from '../services/shopping-list.service';
import { Observable } from 'rxjs';
import { ShoppingList, State } from '../shared/shopping-list.model';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'co-shopping-lists',
  templateUrl: './shopping-lists.component.html',
  styles: []
})
export class ShoppingListsComponent implements OnInit {
  shoppingLists: Observable<Array<ShoppingList>>;

  constructor(private shoppingListsService: ShoppingListService) { }

  ngOnInit(): void {
    this.shoppingLists = this.shoppingListsService.getAll().pipe(
      map(lists => lists.filter(list => list.state === State.offen))
    );
  }

}
